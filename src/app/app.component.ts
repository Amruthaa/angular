import { Component } from '@angular/core';


@Component({
 selector: 'app-root',
 templateUrl: './app.component.html',
 styleUrls: ['./app.component.css']
})
export class AppComponent {
user = { firstname : ''};
name = [];

 add() {
   this.name.push({...this.user});
 }
 delete(i) {
   this.name.splice(i, 1);
 }
}
